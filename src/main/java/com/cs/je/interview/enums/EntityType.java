/**
 * 
 */
package com.cs.je.interview.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Rajendra
 *
 */
public enum EntityType {
	ALERTS("Alerts"), INTERESTS("Interests"), OFFERS("Offers"), NEWS("News"), MEETING("Meeting");
	
	private String value;
	
	private EntityType(String value) {
		this.value = value;
	}
	
	@JsonValue
	public String getValue() {
		return value;
	}
}
