/**
 * 
 */
package com.cs.je.interview.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Rajendra
 *
 */
public enum NotificationType {
	NEW("New"), UPDATE("Update");
	
	private String value;
	
	private NotificationType(String value) {
		this.value = value;
	}
	
	@JsonValue
	public String getValue() {
		return value;
	}

}
