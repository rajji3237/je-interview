/**
 * 
 */
package com.cs.je.interview.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Rajendra
 *
 */
public enum SlotStatus {
	OPEN("Open"), ASSIGNED("Assigned"), COMPLETED("Completed"), CANCELED("Canceled");
	
	private String value;
	
	private SlotStatus(String value) {
		this.value = value;
	}
	
	@JsonValue
	public String getValue() {
		return value;
	}

}
