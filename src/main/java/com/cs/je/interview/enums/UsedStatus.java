/**
 * 
 */
package com.cs.je.interview.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Rajendra
 *
 */
public enum UsedStatus {
	
	CURRENT("Current"), PREVIOUS("Previous");
	
	private String value;
	
	UsedStatus(final String value){
		this.value = value;
	}

	@JsonValue
	public String getValue(){
		return this.value;
	}
}
