/**
 * 
 */
package com.cs.je.interview.enums;

/**
 * @author sawai
 *
 */
public enum InterestJobType {

	FULL_TIME, PART_TIME;
}
