/**
 * 
 */
package com.cs.je.interview.enums;

/**
 * @author sawai
 *
 */
public enum InterestStatus {
	ACCEPT("Accept"), OTHER("Other"), DECLINED("Declined");
	
	private String value;
	
	private InterestStatus(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
