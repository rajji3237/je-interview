/**
 * 
 */
package com.cs.je.interview.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Rajendra
 *
 */
public enum InterviewerSettingKey {
	ALERTS("notification.alerts"), INTERESTS("notification.interests"), OFFERS("notification.offers"), 
	NEWS("notification.news"), EMAIL_NOTIFICATION_TO("notification.emailto");
	
	private String value;
	
	private InterviewerSettingKey(String value) {
		this.value = value;
	}
	
	@JsonValue
	public String getValue() {
		return value;
	}

    public static InterviewerSettingKey byValue(String value) {
        for (InterviewerSettingKey key : InterviewerSettingKey.values()) {
            if (key.value.equals(value)) {
                return key;
            }
        }

        return null;
    }
}
