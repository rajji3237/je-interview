/**
 * 
 */
package com.cs.je.interview.enums;

/**
 * @author Rajendra
 *
 */

public enum Role {
	ROLE_USER("ROLE_USER"), ROLE_ADMIN("ROLE_ADMIN"), ROLE_CANDIDATE("ROLE_CANDIDATE"), ROLE_SUPER_USER("ROLE_SUPER_USER"), ROLE_INTERVIEWER("ROLE_INTERVIEWER"),
	ROLE_JE_STAFF("ROLE_JE_STAFF"), ROLE_JE_ADMIN("ROLE_JE_ADMIN");
	
	private String value;
	
	private Role(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
}