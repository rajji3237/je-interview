/**
 * 
 */
package com.cs.je.interview.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Rajendra
 *
 */
public enum MeetingEntityType {

	INTEREST("Interest"), OFFER("Offer"), INTERVIEW("Interview");
	
	private String value;
	
	private MeetingEntityType(String value) {
		this.value = value;
	}
	
	@JsonValue
	public String getValue() {
		return value;
	}
}
