/**
 * 
 */
package com.cs.je.interview.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Rajendra
 *
 */
public enum AlertsSeverity {
	LOW("Low"), MODERATE("Moderate"), HIGH("High");
	
	private String value;
	
	private AlertsSeverity(String value) {
		this.value = value;
	}
	
	@JsonValue
	public String getValue() {
		return value;
	}
}
