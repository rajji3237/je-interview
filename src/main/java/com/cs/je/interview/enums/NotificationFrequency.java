/**
 * 
 */
package com.cs.je.interview.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Rajendra
 *
 */
public enum NotificationFrequency {
	IMMEDIATE("Immediate"), WEEKLY("Weekly"), DAILY("Daily"), NEVER("Never");
	
	private String value;
	
	private NotificationFrequency(String value) {
		this.value = value;
	}
	
	@JsonValue
	public String getValue() {
		return value;
	}
}
