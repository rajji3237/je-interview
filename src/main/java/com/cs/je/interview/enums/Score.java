/**
 * 
 */
package com.cs.je.interview.enums;

/**
 * @author Rajendra
 *
 */
public enum Score {
	UNSATISFACTORY(1), SATISFACTORY(2), AVERAGE(3), ABOVE_AVERAGE(4), EXCEPTIONAL(5);
	
	private int value;
	
	private Score(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public static Score getByValue(int value){
		Score score = null;
		if(value==1)
			score = Score.UNSATISFACTORY;
		if(value==2)
			score = Score.SATISFACTORY;
		if(value==3)
			score = Score.AVERAGE;
		if(value==4)
			score = Score.ABOVE_AVERAGE;
		if(value==5)
			score = Score.EXCEPTIONAL;
		return score;
	}
}
