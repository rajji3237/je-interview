/**
 * 
 */
package com.cs.je.interview.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author Rajendra
 *
 */
public enum RequestType {
	DECLINE("Decline"), PROPOSE("Propose a New Time");
	
	private String value;
	
	private RequestType(String value) {
		this.value = value;
	}
	
	@JsonValue
	public String getValue() {
		return value;
	}
}
