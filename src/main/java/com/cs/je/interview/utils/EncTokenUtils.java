/**
 * 
 */
package com.cs.je.interview.utils;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;

/**
 * @author sawai
 *
 */

public class EncTokenUtils {
	
	public static void validateAuthorization(String encToken) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		try {
			String key = "Bar12345Bar12345"; // 128 bit key
			Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
		    Cipher cipher = Cipher.getInstance("AES");
		        
		    // decrypt the text
		    cipher.init(Cipher.DECRYPT_MODE, aesKey);
		    byte[] byteArray = Base64.getDecoder().decode(encToken);
		    String emailFromEncToken = new String(cipher.doFinal(byteArray));
		    String loggedInUserEmail = UserUtils.getCurrentUser().getEmail();
		    if (emailFromEncToken == null || loggedInUserEmail == null || !emailFromEncToken.equals(loggedInUserEmail)) {
		    	throw new UnauthorizedUserException("");
		    }
		} catch (Exception e) {
			throw new UnauthorizedUserException("");
		}
	}
}