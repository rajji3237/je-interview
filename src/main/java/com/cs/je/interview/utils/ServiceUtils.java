/**
 * 
 */
package com.cs.je.interview.utils;

import javax.persistence.EntityNotFoundException;

/**
 * @author Rajendra
 *
 */
public class ServiceUtils {
	
	public static void checkForEntities(Object P, Object D) throws IllegalArgumentException, EntityNotFoundException{
		if(P==null)
			throw new IllegalArgumentException("Null object is provided as argument!");
		if(D==null)
			throw new EntityNotFoundException("No object found for provided id!");
	}
	
	public static void checkForEntity(Object P) throws IllegalArgumentException{
		if(P==null)
			throw new IllegalArgumentException("Null object is provided as argument!");
	}

}
