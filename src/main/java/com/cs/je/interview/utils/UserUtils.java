/**
 * 
 */
package com.cs.je.interview.utils;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.cs.je.interview.model.User;

/**
 * @author Rajendra
 *
 */

public class UserUtils {
	
	private static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();
	
	public static User getCurrentUser() {
		return (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
	public static String passwordEncoder(String password) {
		return PASSWORD_ENCODER.encode(password);
	}
	
}
