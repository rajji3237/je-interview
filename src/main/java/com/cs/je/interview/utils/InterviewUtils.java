/**
 * 
 */
package com.cs.je.interview.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.cs.je.interview.enums.SlotStatus;
import com.cs.je.interview.model.InterviewSlot;

/**
 * @author Rajendra
 *
 */
public class InterviewUtils {
	
	public static SlotStatus getInterviewStatus(List<InterviewSlot>interviewSlots){
		int openCount = 0;
		int assignedCount = 0;
		int completedCount = 0;
		for(InterviewSlot interviewSlot : interviewSlots){
			if(!interviewSlot.getStatus().equals(SlotStatus.CANCELED)){
				if(interviewSlot.getStatus().equals(SlotStatus.OPEN)){
					openCount++;
				}
				if(interviewSlot.getStatus().equals(SlotStatus.ASSIGNED)){
					assignedCount++;
				}
				if(interviewSlot.getStatus().equals(SlotStatus.COMPLETED)){
					completedCount++;
				}
			}
		}
		if(openCount>0){
			return SlotStatus.OPEN;
		}
		if(openCount==0 && assignedCount>0)
			return SlotStatus.ASSIGNED;
		if(openCount==0 && assignedCount==0 && completedCount>0)
			return SlotStatus.COMPLETED;
		return SlotStatus.OPEN;
	}
	
	public static Date addMinutes(Date start, int minutes){
		Calendar cal = Calendar.getInstance();
		cal.setTime(start);
		cal.add(Calendar.MINUTE, minutes);
		return cal.getTime();
	}

}
