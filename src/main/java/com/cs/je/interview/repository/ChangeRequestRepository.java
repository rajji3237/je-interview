/**
 * 
 */
package com.cs.je.interview.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.cs.je.interview.model.ChangeRequest;
import com.cs.je.interview.model.InterviewSlot;

/**
 * @author Rajendra
 *
 */
public interface ChangeRequestRepository extends JpaRepository<ChangeRequest, Long> {
	
	public List<ChangeRequest> findByCreatedBy(@Param("createdBy") String createdBy);
	
	public List<ChangeRequest> findByCreatedByAndInterviewSlot(@Param("createdBy") String createdBy, @Param("slot") InterviewSlot interviewSlot);

}
