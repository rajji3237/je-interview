/**
 * 
 */
package com.cs.je.interview.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cs.je.interview.enums.SlotStatus;
import com.cs.je.interview.model.Interview;
import com.cs.je.interview.model.InterviewSlot;

/**
 * @author Rajendra
 *
 */
@Repository
public interface SlotRepository extends JpaRepository<InterviewSlot, Long> {
	
	public List<InterviewSlot> findByInterviewerIdAndStatusOrderByTimeSlot_startTimeAsc(@Param("interviewerId") String interviewerId, @Param("status") SlotStatus status, @Param("page") Pageable page);
	
	public List<InterviewSlot> findDistinctByStatusAndSkills_technologyInOrderByTimeSlot_startTimeAsc(@Param("status") SlotStatus status, @Param("skills") List<String> techologies, @Param("page") Pageable page);
	
	public int countByInterviewerIdAndStatus(@Param("interviewerId") String interviewerId, @Param("status") SlotStatus status);
	
	public int countDistinctByStatusAndSkills_technologyIn(@Param("status") SlotStatus status, @Param("skills") List<String> techologies);
	
	public List<InterviewSlot> findByInterview(@Param("interview") Interview interview);

}
