/**
 * 
 */
package com.cs.je.interview.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cs.je.interview.enums.SlotStatus;
import com.cs.je.interview.model.Interview;

/**
 * @author Rajendra
 *
 */
@Repository
public interface InterviewRepository extends JpaRepository<Interview, Long> {

//	@Query("select i from Interview i where i.status = com.cs.je.interview.enums.InterviewStatus.NOT_CONFIRMED order by i.date asc")
//	public List<Interview> findByStatusNOT_CONFIRMEDOrderByDateAsc();
//
//	@Query("select i from Interview i where i.interviewer = :interviewer  and i.accepted = true and i.status != com.cs.je.interview.enums.InterviewStatus.CANCELED and i.status != com.cs.je.interview.enums.InterviewStatus.COMPLETED")
//	public List<Interview> findByInterviewerAndAcceptedTrueOrderByDateAsc(@Param("interviewer") Interviewer interviewer);
//	
//	@Query("select i from Interview i where i.interviewer = :interviewer and i.accepted = true and i.status = com.cs.je.interview.enums.InterviewStatus.CONFIRMED")
//	public Interview findTopByInterviewerAndStatusConfirmedOrderByDateAsc(@Param("interviewer") Interviewer interviewer);
	
	public List<Interview> findByStatus(@Param("status") SlotStatus status, Pageable pagable);
	
//	@Query("select i from Interview i where :slot in i.slots")
//	public Interview findBySlotsIn(@Param("slot") InterviewSlot slot);

}
