/**
 * 
 */
package com.cs.je.interview.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cs.je.interview.VM.PagedResponse;
import com.cs.je.interview.model.ChangeRequest;
import com.cs.je.interview.model.Interview;
import com.cs.je.interview.model.InterviewSlot;
import com.cs.je.interview.service.InterviewService;
import com.cs.je.interview.service.SlotService;

import io.swagger.annotations.Api;

/**
 * @author Rajendra
 *
 */

@RestController
@RequestMapping(value = "/interview")
@Api(value = "Interviewer Controller")
public class InterviewController {
	
	@Autowired
	private InterviewService interviewService;
	
	@Autowired
	private SlotService slotService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Interview getInterview(@PathVariable("id") Long id) throws Exception {		
		return interviewService.getById(id);
	}
	
//	@RequestMapping(value = "/interview/request", method = RequestMethod.GET)
//	public List<Interview> getInterviewRequests() throws Exception {		
//		return interviewService.getInterviewRequests();
//	}
//	
//	@RequestMapping(value = "/interview/schedules", method = RequestMethod.GET)
//	public List<Interview> getInterviewShedules() throws Exception {		
//		return interviewService.getInterviewSchedules();
//	}
//	
//	@RequestMapping(value = "/interview/next", method = RequestMethod.GET)
//	public Interview getNextInterview() throws Exception {		
//		return interviewService.getNextInterview();
//	}
	
	@RequestMapping(value = "", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Interview createInterview(@RequestBody Interview interview, HttpServletRequest request) throws Exception {
		//commented for testing only
//		String encToken = request.getHeader("Enc-Token");
//		EncTokenUtils.validateAuthorization(encToken);
		return interviewService.create(interview);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Interview updateInterview(@PathVariable("id") Long id, @RequestBody Interview interview) throws Exception {		
		return interviewService.update(id, interview);
	}
	
	@RequestMapping(value = "/slot/{id}", method = RequestMethod.GET)
	public InterviewSlot getSlot(@PathVariable("id") Long id) throws Exception {		
		return slotService.getById(id);
	}
	
	@RequestMapping(value = "/slot/scheduled", method = RequestMethod.GET)
	public PagedResponse<InterviewSlot> getScheduledSlots(@RequestParam("size") int size, @RequestParam("page") int page) throws Exception {		
		return slotService.getScheduledSlots(size, page);
	}
	
	@RequestMapping(value = "/slot/{ids}/accept", method = RequestMethod.PUT)
	public List<InterviewSlot> acceptSlot(@PathVariable("ids") Long[] ids) throws Exception {		
		return slotService.acceptSlot(ids);
	}
	
	@RequestMapping(value = "/slot/{id}/cancel", method = RequestMethod.GET)
	public InterviewSlot cancelSlots(@PathVariable(name="id") Long id) throws Exception {		
		return slotService.cancelSlot(id);
	}
	
	@RequestMapping(value = "/slot/{id}/approve", method = RequestMethod.GET)
	public InterviewSlot approveChanges(@PathVariable(name="id") Long id) throws Exception {		
		return slotService.approveChanges(id);
	}
	
	@RequestMapping(value = "/slot/{id}/change", method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE)
	public InterviewSlot changeRequest(@PathVariable(name="id") Long slotId, @RequestBody ChangeRequest changeRequest) throws Exception {		
		return slotService.requestChange(slotId, changeRequest);
	}
	
	@RequestMapping(value = "/slot/{id}/rejectChanges", method = RequestMethod.GET)
	public InterviewSlot rejectSlotChages(@PathVariable(name="id") Long id) throws Exception {		
		return slotService.rejectChange(id);
	}

}
