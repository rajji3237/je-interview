/**
 * 
 */
package com.cs.je.interview.config.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.savedrequest.NullRequestCache;

import com.cs.je.interview.filter.CORSFilter;

@Configuration 
@EnableResourceServer
public class ResourceConfig extends ResourceServerConfigurerAdapter {
    
	@Value("${security.oauth2.resource.id}")
    private String resourceId;

    @Autowired
    private DefaultTokenServices tokenServices;

    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private CORSFilter corsFilter;
    
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources
                .resourceId(resourceId)
                .tokenServices(tokenServices)
                .tokenStore(tokenStore);
    }
    
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
        .authorizeRequests().antMatchers("/").permitAll().and().authorizeRequests().antMatchers("/console/**")
		.permitAll().and().authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/**").permitAll().and()
         .authorizeRequests()
                
                // when restricting access to 'Roles' you must remove the "ROLE_" part role
                // for "ROLE_USER" use only "USER"
                .antMatchers(HttpMethod.POST ,"/api/interviewer/**").hasAnyAuthority("ROLE_SUPER_USER", "ROLE_ADMIN")
                //.antMatchers("/api/hello").access("hasAnyRole('USER')")          
                //.antMatchers("/api/admin").hasRole("ADMIN")
                // restricting all access to /api/** to authenticated users
                .anyRequest().authenticated();
        
        http.csrf().disable();
		http.headers().frameOptions().disable();
		http.requestCache().requestCache(new NullRequestCache());
		http.httpBasic();
	    http.addFilterBefore(corsFilter, ChannelProcessingFilter.class);
	}
}