package com.cs.je.interview.exceptions;

public class SlotNotAvailableException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1300369742144171347L;

	public SlotNotAvailableException(Long id1, Long id2) {
		super("This slot ("+id1+") cann't be taken, because its falling in slot ("+id2+") duration!");
	}

}
