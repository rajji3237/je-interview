/**
 * 
 */
package com.cs.je.interview.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * @author Rajendra
 *
 */
@Entity
@Table(name="interview_template")
@TableGenerator(name = "template_id_gen", initialValue = 1, allocationSize =1)
public class InterviewTemplate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2541398456600671715L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "template_id_gen")
	private Long id;
	
	@Column
	private String name;

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="interviewTemplate")
    @JsonManagedReference
	private List<SlotTemplate> slotTemplates;
	
	public InterviewTemplate(){
		
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the slotTemplates
	 */
	public List<SlotTemplate> getSlots() {
		return slotTemplates;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param slotTemplates the slotTemplates to set
	 */
	public void setSlots(List<SlotTemplate> slotTemplates) {
		this.slotTemplates = slotTemplates;
	}
}
