/**
 * 
 */
package com.cs.je.interview.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
//import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.cs.je.interview.audit.AbstractEntityAuditable;
import com.cs.je.interview.enums.Score;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Rajendra
 *
 */
@Entity
@Table(name = "Response")
@TableGenerator(name = "response_id_gen", initialValue = 1, allocationSize =1)
public class Response extends AbstractEntityAuditable<User, String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6434166487465051913L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "response_id_gen")
	@Column
	private Long id;
	
//	@OneToOne
//	@JoinColumn(name="question", insertable = true)
	@Column
	private String question;
	
	@Enumerated(EnumType.STRING)
	@Column
	private Score score;
	
	@Column
	private String comments;
	
	@ManyToOne
	@JoinColumn(name="interview_slot")
	@JsonIgnore
	private InterviewSlot interviewSlot;


	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}


	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}


	/**
	 * @return the score
	 */
	public Score getScore() {
		return score;
	}


	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}


	/**
	 * @return the interviewSlot
	 */
	public InterviewSlot getSlot() {
		return interviewSlot;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}


	/**
	 * @param score the score to set
	 */
	public void setScore(Score score) {
		this.score = score;
	}


	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}


	/**
	 * @param interviewSlot the interviewSlot to set
	 */
	public void setSlot(InterviewSlot interviewSlot) {
		this.interviewSlot = interviewSlot;
	}

}
