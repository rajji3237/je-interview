/**
 * 
 */
package com.cs.je.interview.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.springframework.format.annotation.DateTimeFormat;

import com.cs.je.interview.audit.AbstractEntityAuditable;
import com.cs.je.interview.enums.RequestType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rajendra
 *
 */
@Entity
@Table(name="change_request")
@TableGenerator(name = "cr_id_gen", initialValue = 1, allocationSize =1)
public class ChangeRequest extends AbstractEntityAuditable<User, String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 543759284607019443L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "cr_id_gen")
	@Column
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="request_type")
	private RequestType requestType;
	
	@Column
	private String comments;
	
	@Column
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date proposedTime;
	
	@Column
	private boolean approved;
	
	@OneToOne(mappedBy="changeRequest")
	@JsonIgnore
	private InterviewSlot interviewSlot;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @return the interviewSlot
	 */
	public InterviewSlot getSlot() {
		return interviewSlot;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @param interviewSlot the interviewSlot to set
	 */
	public void setSlot(InterviewSlot interviewSlot) {
		this.interviewSlot = interviewSlot;
	}

	/**
	 * @return the requestType
	 */
	public RequestType getRequestType() {
		return requestType;
	}

	/**
	 * @return the proposedTime
	 */
	public Date getProposedTime() {
		return proposedTime;
	}

	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

	/**
	 * @param proposedTime the proposedTime to set
	 */
	public void setProposedTime(Date proposedTime) {
		this.proposedTime = proposedTime;
	}

	/**
	 * @return the approved
	 */
	@JsonProperty
	public boolean isApproved() {
		return approved;
	}

	/**
	 * @param approved the approved to set
	 */
	@JsonIgnore
	public void setApproved(boolean approved) {
		this.approved = approved;
	}

}
