/**
 * 
 */
package com.cs.je.interview.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.springframework.format.annotation.DateTimeFormat;

import com.cs.je.interview.audit.AbstractEntityAuditable;
import com.cs.je.interview.enums.Score;
import com.cs.je.interview.enums.SlotStatus;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rajendra
 *
 */
@Entity
@Table(name = "Interview")
@TableGenerator(name = "interview_id_gen", initialValue = 1, allocationSize =1)
public class Interview extends AbstractEntityAuditable<User, String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6847065554056415479L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "interview_id_gen")
	@Column(name="id")
	private Long id;

	@Column(name="start_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date startDate;

	@Column(name="candidate_titile")
	private String candidateTitle;

	@Column(name="candidate_id", nullable=false)
	private String candidateId;
	
	@Column(name="duration")
	private Integer duration;
	
	@Column(name="average_score")
	@Enumerated(EnumType.STRING)
	private Score averageScore;
	
	@Column(name="status")
	@Enumerated(EnumType.STRING)
	private SlotStatus status;

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="interview")
    @JsonManagedReference
//	@JoinTable(name="interview_slots", joinColumns = @JoinColumn(name = "interview_id", insertable=true, updatable=true))
	private List<InterviewSlot> interviewSlots;
	
	public Interview(){
		
	}
	
	public Interview(Long id){
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the date
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @return the candidateTitle
	 */
	public String getCandidateTitle() {
		return candidateTitle;
	}

	/**
	 * @return the duration
	 */
	@JsonProperty
	public Integer getDuration() {
		return duration;
	}

	/**
	 * @return the status
	 */
	@JsonProperty
	public SlotStatus getStatus() {
		return status;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param date the date to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @param candidateTitle the candidateTitle to set
	 */
	public void setCandidateTitle(String candidateTitle) {
		this.candidateTitle = candidateTitle;
	}

//	/**
//	 * @param loginTime the loginTime to set
//	 */
//	public void setLoginTime(Date loginTime) {
//		this.loginTime = loginTime;
//	}

	/**
	 * @param duration the duration to set
	 */
	@JsonIgnore
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	/**
	 * @param status the status to set
	 */
	@JsonIgnore
	public void setStatus(SlotStatus status) {
		this.status = status;
	}

	/**
	 * @return the interviewSlots
	 */
	public List<InterviewSlot> getSlots() {
		return interviewSlots;
	}

	/**
	 * @param interviewSlots the interviewSlots to set
	 */
	@JsonBackReference
	public void setSlots(List<InterviewSlot> interviewSlots) {
		this.interviewSlots = interviewSlots;
	}

	/**
	 * @return the averageScore
	 */
	@JsonProperty
	public Score getAverageScore() {
		return averageScore;
	}

	/**
	 * @param averageScore the averageScore to set
	 */
	@JsonIgnore
	public void setAverageScore(Score averageScore) {
		this.averageScore = averageScore;
	}

	/**
	 * @return the candidateId
	 */
	@JsonIgnore
	public String getCandidateId() {
		return candidateId;
	}

	/**
	 * @param candidateId the candidateId to set
	 */
	@JsonProperty
	public void setCandidateId(String candidateId) {
		this.candidateId = candidateId;
	}
}
