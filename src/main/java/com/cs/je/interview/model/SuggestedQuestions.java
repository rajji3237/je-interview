/**
 * 
 */
package com.cs.je.interview.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.cs.je.interview.enums.InterviewType;

/**
 * @author Rajendra
 *
 */
@Entity
@Table(name = "Suggested_Questions")
@TableGenerator(name = "suggested_questions_id_gen", initialValue = 1, allocationSize =1)
public class SuggestedQuestions implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3916400930224054786L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "suggested_questions_id_gen")
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="interview_type", unique=true)
	private InterviewType type;
	
	@OneToMany
	@JoinTable(name="suggested_question_questions")
	private List<Question> questions;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the type
	 */
	public InterviewType getType() {
		return type;
	}

	/**
	 * @return the questions
	 */
	public List<Question> getQuestions() {
		return questions;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(InterviewType type) {
		this.type = type;
	}

	/**
	 * @param questions the questions to set
	 */
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	
	

}
