/**
 * 
 */
package com.cs.je.interview.model;

import java.io.Serializable;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author Rajendra
 *
 */
@Entity
@Table(name = "Time_Slot")
@TableGenerator(name = "time_slot_id_gen", initialValue = 1, allocationSize =1)
public class TimeSlot implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8839048798468054890L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "time_slot_id_gen")
	private Long id;
	
	@Column(name="start_time")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date startTime;

	@Column(name="end_time")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date endTime;
	
	@Column
	private TimeZone candidateTimeZone;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the startTime
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @return the endTime
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * @return the candidateTimeZone
	 */
	public TimeZone getCandidateTimeZone() {
		return candidateTimeZone;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * @param candidateTimeZone the candidateTimeZone to set
	 */
	public void setCandidateTimeZone(TimeZone candidateTimeZone) {
		this.candidateTimeZone = candidateTimeZone;
	}

}
