/**
 * 
 */
package com.cs.je.interview.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.cs.je.interview.enums.UsedStatus;

/**
 * @author Rajendra
 *
 */
@Entity
@Table(name = "Skills")
@TableGenerator(name = "skill_id_gen", initialValue = 1, allocationSize =1)
public class Skills implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -367347252036769298L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "skill_id_gen")
	@Column(name="id")
	private Long id;
	
	@Column(nullable=false)
	private String technology;
	
	@Column(nullable=false)
	private Float experiance;
	
	@Enumerated(EnumType.STRING)
	private UsedStatus status;
	
	public Skills(){
		
	}
	
	public Skills(final Long id){
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the technology
	 */
	public String getTechnology() {
		return technology;
	}

	/**
	 * @param technology the technology to set
	 */
	public void setTechnology(String technology) {
		this.technology = technology;
	}

	/**
	 * @return the experiance
	 */
	public Float getExperiance() {
		return experiance;
	}

	/**
	 * @param experiance the experiance to set
	 */
	public void setExperiance(Float experiance) {
		this.experiance = experiance;
	}

	/**
	 * @return the status
	 */
	public UsedStatus getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(UsedStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Skills [id=" + id + ", technology=" + technology + ", experience="
				+ experiance + ", status=" + status + "]";
	}

}
