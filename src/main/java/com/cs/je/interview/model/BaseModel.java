/**
 * 
 */
package com.cs.je.interview.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.cs.je.interview.audit.AbstractEntityAuditable;

/**
 * @author sawai
 *
 */

@MappedSuperclass
abstract public class BaseModel extends AbstractEntityAuditable<User, Long> implements Serializable {

	private static final long serialVersionUID = 4318348077188858221L;
	
	@Column(nullable = false)
	private String interviewerId;

	/**
	 * @return the interviewerId
	 */
	public String getInterviewerId() {
		return interviewerId;
	}

	/**
	 * @param interviewerId the interviewerId to set
	 */
	public void setInterviewerId(String interviewerId) {
		this.interviewerId = interviewerId;
	}

	@Override
	public String toString() {
		return "BaseModel [interviewerId=" + interviewerId + "]";
	}
}
