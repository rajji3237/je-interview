/**
 * 
 */
package com.cs.je.interview.model;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rajendra
 *
 */

@SuppressWarnings("serial")
@MappedSuperclass
public class BaseSlot implements Serializable{
	
	Long originalSlotId;

	/**
	 * @return the originalSlotId
	 */
	@JsonProperty
	public Long getOriginalSlotId() {
		return originalSlotId;
	}

	/**
	 * @param originalSlotId the originalSlotId to set
	 */
	@JsonIgnore
	public void setOriginalSlotId(Long originalSlotId) {
		this.originalSlotId = originalSlotId;
	}

}
