/**
 * 
 */
package com.cs.je.interview.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Rajendra
 *
 */
@Entity
@Table(name="question")
public class Question implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1970640571980347253L;
	
	@Id
	private String question;
	
	public Question(){
		
	}
	
	public Question(String question){
		this.question = question;
	}

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

}
