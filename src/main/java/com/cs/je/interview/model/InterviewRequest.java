/**
 * 
 */
package com.cs.je.interview.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Rajendra
 *
 */
public class InterviewRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1018970600209038239L;
	
	private String candidateId;
	
	
	
	private Date lastInterviewDate;
	
	private List<Skills> skills;
	
	private String notes;

	/**
	 * @return the candidateId
	 */
	public String getCandidateId() {
		return candidateId;
	}

	/**
	 * @return the lastInterviewDate
	 */
	public Date getLastInterviewDate() {
		return lastInterviewDate;
	}

	/**
	 * @return the skills
	 */
	public List<Skills> getSkills() {
		return skills;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param candidateId the candidateId to set
	 */
	public void setCandidateId(String candidateId) {
		this.candidateId = candidateId;
	}

	/**
	 * @param lastInterviewDate the lastInterviewDate to set
	 */
	public void setLastInterviewDate(Date lastInterviewDate) {
		this.lastInterviewDate = lastInterviewDate;
	}

	/**
	 * @param skills the skills to set
	 */
	public void setSkills(List<Skills> skills) {
		this.skills = skills;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

}
