/**
 * 
 */
package com.cs.je.interview.model;

import java.util.Collection;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.cs.je.interview.audit.AbstractEntityAuditable;
import com.cs.je.interview.enums.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
 
/**
 * @author Rajendra
 *
 */

@Entity
@Table(name = "USER")
@Inheritance(strategy = InheritanceType.JOINED)
public class User extends AbstractEntityAuditable<User, String> implements UserDetails {

	private static final long serialVersionUID = 2923767651859585283L;

	@Id 
	private String id;
	
	@Column
	private String firstName;
	
	@Column
	private String lastName;
	
	@Enumerated(EnumType.STRING)
	private Role role;

    @Column(nullable=false, unique = true)
	private String email;
	
	@Transient
	private Set<GrantedAuthority> authorities;
	
	@Transient
	private String tenantId;
	
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setAuthorities(Set<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	@JsonIgnore
	public String getPassword() {
		return null;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getUsername() {
		return email;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName="
				+ lastName + ", role=" + role + ", email=" + email
				+ ", authorities=" + authorities + ", tenantId=" + tenantId
				+ "]";
	}
}