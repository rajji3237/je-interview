/**
 * 
 */
package com.cs.je.interview.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.cs.je.interview.enums.InterviewType;

/**
 * @author Rajendra
 *
 */
@Entity
@Table(name = "Interview_Tip")
@TableGenerator(name = "interview_tip_id_gen", initialValue = 1, allocationSize =1)
public class InterviewTip implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6670343496480389248L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "interview_tip_id_gen")
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name="interview_type", unique=true)
	private InterviewType type;
	
	@Column(name="tip", length=10000)
	private String tip;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the type
	 */
	public InterviewType getType() {
		return type;
	}

	/**
	 * @return the tip
	 */
	public String getTip() {
		return tip;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(InterviewType type) {
		this.type = type;
	}

	/**
	 * @param tip the tip to set
	 */
	public void setTip(String tip) {
		this.tip = tip;
	}

}
