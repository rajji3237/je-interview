/**
 * 
 */
package com.cs.je.interview.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * @author Rajendra
 *
 */
@Entity
@Table(name="slot_template")
@TableGenerator(name = "slot_id_gen", initialValue = 1, allocationSize =1)
public class SlotTemplate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4589398436385795379L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "slot_id_gen")
	private Long id;

	@Column
	private String name;
	
	@Column
	private Integer duration;

	@ManyToOne
	@JoinColumn(name="interviewTemplate")
	@JsonBackReference
	private InterviewTemplate interviewTemplate;
	
	public SlotTemplate(){
		
	}
	
	public SlotTemplate(Long id){
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the duration
	 */
	public Integer getDuration() {
		return duration;
	}

	/**
	 * @return the interviewTemplate
	 */
	public InterviewTemplate getTemplate() {
		return interviewTemplate;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	/**
	 * @param interviewTemplate the interviewTemplate to set
	 */
	public void setTemplate(InterviewTemplate interviewTemplate) {
		this.interviewTemplate = interviewTemplate;
	}
}
