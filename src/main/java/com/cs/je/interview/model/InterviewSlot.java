/**
 * 
 */
package com.cs.je.interview.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.cs.je.interview.enums.InterviewType;
import com.cs.je.interview.enums.Score;
import com.cs.je.interview.enums.SlotStatus;
import com.cs.je.interview.utils.InterviewUtils;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rajendra
 *
 */
@Entity
@Table(name = "Interview_Slot")
@TableGenerator(name = "interview_slot_id_gen", initialValue = 1, allocationSize =1)
public class InterviewSlot extends BaseSlot{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7745652633572610699L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "interview_slot_id_gen")
	@Column(name="id")
	private Long id;
	
	@Transient
	private int slotNumber;

	@Column(name="interview_type")
	@Enumerated(EnumType.STRING)
	private InterviewType interviewType;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="time_slot", nullable=false, insertable=true, updatable=true)
	private TimeSlot timeSlot;

	@Transient
	private String candidate;
	
	@OneToOne
	@JoinColumn(name="interview_tip_id")
	private InterviewTip interviewTips;

    @OneToMany(cascade=CascadeType.ALL)
	@JoinTable(name="slot_skills", joinColumns = @JoinColumn(name = "slot_id", insertable=true, updatable=true))
    @Size(min=1, max=2)
	private List<Skills> skills;

	@Column(nullable=false)
	@Enumerated(EnumType.STRING)
	private SlotStatus status;

	@Column
	private Score score;

	@Column
	private String comments;
	
	@Column
	private String notes;
	
	@Column(name="interviewer_id")
	private String interviewerId;

	@Transient
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date loginTime;
	
	@Column(name="vedio_link")
	private String vedioLink;
	
	@OneToOne
	@JoinColumn(name="suggested_questions_id")
	private SuggestedQuestions suggestedQuestions;
	
	@OneToMany(mappedBy = "interviewSlot", cascade=CascadeType.ALL)
	private List<Response> responses;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="change_request", insertable=true, updatable=true)
	private ChangeRequest changeRequest;
	
	@ManyToOne
	@JoinColumn(name="interview")
	@JsonBackReference
	private Interview interview;

	public InterviewSlot(){
		
	}
	
	public InterviewSlot(Long id){
		
	}
	
	public InterviewSlot(InterviewSlot interviewSlot){
		this.originalSlotId = interviewSlot.originalSlotId==null?interviewSlot.id: interviewSlot.originalSlotId;
		this.timeSlot = interviewSlot.timeSlot;
		this.skills = interviewSlot.skills;
		this.status = SlotStatus.OPEN;
//		this.questions = slot.getQuestions();
		this.vedioLink = interviewSlot.vedioLink;
		this.suggestedQuestions = interviewSlot.suggestedQuestions;
		this.interview = interviewSlot.interview;
	}


	/**
	 * @return the loginTime
	 */
	public Date getLoginTime() {
		return this.status.equals(SlotStatus.ASSIGNED)?InterviewUtils.addMinutes(timeSlot.getStartTime(), -15) : null;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the skills
	 */
	public List<Skills> getSkills() {
		return skills;
	}

	/**
	 * @return the status
	 */
	public SlotStatus getStatus() {
		return status;
	}

	/**
	 * @return the score
	 */
	@JsonProperty
	public Score getScore() {
		return score;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @return the interviewerId
	 */
	public String getInterviewerId() {
		return interviewerId;
	}

	/**
	 * @return the interview
	 */
	public Interview getInterview() {
		return interview;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param skills the skills to set
	 */
	public void setSkills(List<Skills> skills) {
		this.skills = skills;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(SlotStatus status) {
		this.status = status;
	}

	/**
	 * @param score the score to set
	 */
	@JsonIgnore
	public void setScore(Score score) {
		this.score = score;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @param interviewerId the interviewerId to set
	 */
	public void setInterviewerId(String interviewerId) {
		this.interviewerId = interviewerId;
	}

	/**
	 * @param interview the interview to set
	 */
	public void setInterview(Interview interview) {
		this.interview = interview;
	}

//	/**
//	 * @return the questions
//	 */
//	public List<String> getQuestions() {
//		return questions;
//	}

	/**
	 * @return the vedioLink
	 */
	public String getVedioLink() {
		return vedioLink;
	}

//	/**
//	 * @param questions the questions to set
//	 */
//	public void setQuestions(List<String> questions) {
//		this.questions = questions;
//	}

	/**
	 * @param vedioLink the vedioLink to set
	 */
	public void setVedioLink(String vedioLink) {
		this.vedioLink = vedioLink;
	}

	/**
	 * @return the suggestedQuestions
	 */
	@JsonProperty
	public SuggestedQuestions getSuggestedQuestions() {
		return suggestedQuestions;
	}

	/**
	 * @return the responses
	 */
	public List<Response> getResponses() {
		return responses;
	}

	/**
	 * @param suggestedQuestions the suggestedQuestions to set
	 */
	@JsonIgnore
	public void setSuggestedQuestions(SuggestedQuestions suggestedQuestions) {
		this.suggestedQuestions = suggestedQuestions;
	}

	/**
	 * @param responses the responses to set
	 */
	public void setResponses(List<Response> responses) {
		responses = this.status.equals(SlotStatus.COMPLETED)?responses: null;
		this.responses = responses;
	}

	/**
	 * @return the changeRequest
	 */
	public ChangeRequest getChangeRequest() {
		return changeRequest;
	}

	/**
	 * @param changeRequest the changeRequest to set
	 */
	public void setChangeRequest(ChangeRequest changeRequest) {
		this.changeRequest = changeRequest;
	}

	/**
	 * @return the candidate
	 */
	public String getCandidate() {
		return interview.getCandidateTitle();
	}

	/**
	 * @return the slotNumber
	 */
	public int getSlotNumber() {
		return interview.getSlots().indexOf(this);
	}

	/**
	 * @return the interviewType
	 */
	public InterviewType getInterviewType() {
		return interviewType;
	}

	/**
	 * @return the interviewTips
	 */
	@JsonProperty
	public InterviewTip getInterviewTips() {
		return interviewTips;
	}

	/**
	 * @param interviewType the interviewType to set
	 */
	public void setInterviewType(InterviewType interviewType) {
		this.interviewType = interviewType;
	}

	/**
	 * @param interviewTips the interviewTips to set
	 */
	@JsonIgnore
	public void setInterviewTips(InterviewTip interviewTips) {
		this.interviewTips = interviewTips;
	}

	/**
	 * @return the timeSlot
	 */
	public TimeSlot getTimeSlot() {
		return timeSlot;
	}

	/**
	 * @param timeSlot the timeSlot to set
	 */
	public void setTimeSlot(TimeSlot timeSlot) {
		this.timeSlot = timeSlot;
	}

}
