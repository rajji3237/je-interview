/**
 * 
 */
package com.cs.je.interview.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author Rajendra
 *
 */
@Entity
@Table(name = "Work_Experience")
@TableGenerator(name = "work_exp_id_gen", initialValue = 1, allocationSize =1)
public class WorkExperience implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 451884202447307210L;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "work_exp_id_gen")
	@Column(name="id")
	private Long id;

	@Column(nullable=false)
	private String designation;

	@Column(nullable=false)
	private String company;

	@Column(name="from_date", nullable=false)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date from;

	@Column(name="to_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date to;
	
	@Column(name="current_emp")
	private boolean currentEmp;
	
	public WorkExperience(){
		
	}
	
	public WorkExperience(final Long id){
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the currentEmp
	 */
	public boolean isCurrentEmp() {
		return currentEmp;
	}

	/**
	 * @param currentEmp the currentEmp to set
	 */
	public void setCurrentEmp(boolean currentEmp) {
		this.currentEmp = currentEmp;
	}

	@Override
	public String toString() {
		return "WorkExperience [id=" + id + ", designation=" + designation + ", company="
				+ company + ", from=" + from + ", to="
				+ to + ", currentEmployer=" + currentEmp + "]";
	}

}
