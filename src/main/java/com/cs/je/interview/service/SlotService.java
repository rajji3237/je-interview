/**
 * 
 */
package com.cs.je.interview.service;

import java.util.Date;
import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import com.cs.je.interview.VM.PagedResponse;
import com.cs.je.interview.exceptions.SlotNotAvailableException;
import com.cs.je.interview.model.ChangeRequest;
import com.cs.je.interview.model.InterviewSlot;

/**
 * @author Rajendra
 *
 */
public interface SlotService extends BaseService<InterviewSlot, Long> {

	@PreAuthorize("hasRole('INTERVIEWER')")
	public PagedResponse<InterviewSlot> getScheduledSlots(int page, int size);

	@PreAuthorize("hasRole('INTERVIEWER')")
	public List<InterviewSlot> acceptSlot(Long[] slotIds) throws SlotNotAvailableException;

	@PreAuthorize("hasRole('INTERVIEWER')")
	public InterviewSlot requestChange(Long slotId, ChangeRequest changeRequest);
	
//	public List<InterviewSlot> getSlotsByInterview(Long interviewId) throws Exception;
	
	@PreAuthorize("hasRole('JE_STAFF')")
	public InterviewSlot cancelSlot(Long slotId) throws Exception;
	
	@PreAuthorize("hasRole('JE_STAFF')")
	public InterviewSlot approveChanges(Long slotId) throws Exception;

	@PreAuthorize("hasRole('JE_STAFF')")
	public InterviewSlot changeTiming(Long slotId, Date newDate);

	@PreAuthorize("hasRole('JE_STAFF')")
	public InterviewSlot rejectChange(Long slotId);

}
