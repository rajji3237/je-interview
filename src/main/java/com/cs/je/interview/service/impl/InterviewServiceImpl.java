/**
 * 
 */
package com.cs.je.interview.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.cs.je.interview.enums.SlotStatus;
import com.cs.je.interview.model.Interview;
import com.cs.je.interview.repository.InterviewRepository;
import com.cs.je.interview.service.InterviewService;
import com.cs.je.interview.utils.ServiceUtils;

/**
 * @author Rajendra
 *
 */
@Service
public class InterviewServiceImpl implements InterviewService {
	
	@Autowired
	private InterviewRepository repo;

	/* (non-Javadoc)
	 * @see com.cs.je.interview.service.BaseService#create(java.lang.Object)
	 */
	@Override
	public Interview create(Interview entity) throws Exception {
		ServiceUtils.checkForEntity(entity);
		SlotStatus status = SlotStatus.OPEN;
		int duration = 0;
//		for(InterviewSlot interviewSlot : entity.getSlots()){
//			if(interviewSlot.getDuration()!=null){
//				duration+=interviewSlot.getDuration();
//			}
//		}
		entity.setStatus(status);
		entity.setDuration(duration);
		return repo.save(entity);
	}

	/* (non-Javadoc)
	 * @see com.cs.je.interview.service.BaseService#getById(java.io.Serializable)
	 */
	@Override
	public Interview getById(Long key) {
		return repo.findOne(key);
	}

	/* (non-Javadoc)
	 * @see com.cs.je.interview.service.BaseService#update(java.io.Serializable, java.lang.Object)
	 */
	@Override
	public Interview update(Long key, Interview entity) throws Exception {
		Interview interview = repo.getOne(key);
		if(entity.getCandidateTitle()!=null){
			interview.setCandidateTitle(entity.getCandidateTitle());
		}
		if(entity.getStartDate()!=null){
			interview.setStartDate(entity.getStartDate());
		}
		if(entity.getDuration()!=null){
			interview.setDuration(entity.getDuration());
		}
		return repo.save(interview);
	}

	/* (non-Javadoc)
	 * @see com.cs.je.interview.service.BaseService#delete(java.io.Serializable)
	 */
	@Override
	public void delete(Long key) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.cs.je.interview.service.BaseService#getAll()
	 */
	@Override
	public List<Interview> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Interview> getInterviewByStatus(String status, int currentPage, int itemsPerPage) {
		SlotStatus interviewStatus = status!=null?SlotStatus.valueOf(status.trim().toUpperCase()):null;
		PageRequest pageRequest = new PageRequest(currentPage, itemsPerPage);
		return repo.findByStatus(interviewStatus, pageRequest);
	}

}
