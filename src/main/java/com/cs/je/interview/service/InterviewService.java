/**
 * 
 */
package com.cs.je.interview.service;

import java.util.List;

import com.cs.je.interview.model.Interview;

/**
 * @author Rajendra
 *
 */
public interface InterviewService extends BaseService<Interview, Long> {
	
	public List<Interview> getInterviewByStatus(String status, int currentPage, int itemsPerPage);

}
