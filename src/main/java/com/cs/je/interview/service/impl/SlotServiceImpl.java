/**
 * 
 */
package com.cs.je.interview.service.impl;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cs.je.interview.VM.PagedResponse;
import com.cs.je.interview.enums.RequestType;
import com.cs.je.interview.enums.Score;
import com.cs.je.interview.enums.SlotStatus;
import com.cs.je.interview.exceptions.SlotNotAvailableException;
import com.cs.je.interview.model.ChangeRequest;
import com.cs.je.interview.model.Interview;
import com.cs.je.interview.model.Response;
import com.cs.je.interview.model.InterviewSlot;
import com.cs.je.interview.repository.InterviewRepository;
import com.cs.je.interview.repository.SlotRepository;
import com.cs.je.interview.service.SlotService;
import com.cs.je.interview.utils.InterviewUtils;
import com.cs.je.interview.utils.ServiceUtils;
import com.cs.je.interview.utils.UserUtils;

/**
 * @author Rajendra
 *
 */
@Service
public class SlotServiceImpl implements SlotService {
	
	@Autowired
	private SlotRepository repo;
	
	@Autowired
	private InterviewRepository interviewRepo;

	/* (non-Javadoc)
	 * @see com.cs.je.interview.service.BaseService#create(java.lang.Object)
	 */
	@Override
	public InterviewSlot create(InterviewSlot entity) throws Exception {
		return repo.save(entity);
	}

	/* (non-Javadoc)
	 * @see com.cs.je.interview.service.BaseService#getById(java.io.Serializable)
	 */
	@Override
	public InterviewSlot getById(Long key) {
		return repo.findOne(key);
	}

	/* (non-Javadoc)
	 * @see com.cs.je.interview.service.BaseService#update(java.io.Serializable, java.lang.Object)
	 */
	@Override
	@Transactional
	public InterviewSlot update(Long key, InterviewSlot entity) throws Exception {
		InterviewSlot interviewSlot = repo.findOne(key);
		boolean statusChanged = false;
		ServiceUtils.checkForEntities(entity, interviewSlot);
		Interview interview = interviewRepo.findOne(interviewSlot.getInterview().getId());
//		if(entity.getChangeRequest()!=null){
//			slot.setChangeRequest(entity.getChangeRequest());
//		}
		if(entity.getComments()!=null){
			interviewSlot.setComments(entity.getComments());
		}
//		if(entity.getQuestions()!=null){
//			slot.setQuestions(entity.getQuestions());
//		}
		if(entity.getNotes()!=null){
			interviewSlot.setNotes(entity.getNotes());
		}
		if(entity.getResponses()!=null){
			interviewSlot.setResponses(entity.getResponses());
			int totalScore = 0;
			for(Response resp : interviewSlot.getResponses()){
				totalScore+=resp.getSlot().getScore().getValue();
			}
			//need to check
			int avgScore = Math.round(totalScore/interviewSlot.getResponses().size());
			interviewSlot.setScore(Score.getByValue(avgScore));
			int totalInterviewScore=0;
			int i=0;
			for(InterviewSlot slotInt : interview.getSlots()){
				if(interviewSlot.getScore()!=null){
					totalInterviewScore+=slotInt.getScore().getValue();
					i++;
				}
			}
			int avgInterviewScore=Math.round(totalInterviewScore/i);
			interview.setAverageScore(Score.getByValue(avgInterviewScore));
		}
		if(entity.getStatus()!=null){
			interviewSlot.setStatus(entity.getStatus());
			statusChanged=true;
		}
		interviewSlot = repo.save(interviewSlot);
		interview = interviewRepo.save(interview);
		if(statusChanged){
			interview.setStatus(InterviewUtils.getInterviewStatus(interview.getSlots()));
			interview = interviewRepo.save(interview);
		}
		return interviewSlot;
	}

	/* (non-Javadoc)
	 * @see com.cs.je.interview.service.BaseService#delete(java.io.Serializable)
	 */
	@Override
	public void delete(Long key) {
		repo.delete(key);

	}

	/* (non-Javadoc)
	 * @see com.cs.je.interview.service.BaseService#getAll()
	 */
	@Override
	public List<InterviewSlot> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.cs.je.interview.service.SlotService#getScheduledSlots()
	 */
	@Override
	public PagedResponse<InterviewSlot> getScheduledSlots(int size, int page) {
		PageRequest pageReq = new PageRequest(page, size);
		List<InterviewSlot> list = repo.findByInterviewerIdAndStatusOrderByTimeSlot_startTimeAsc(UserUtils.getCurrentUser().getId(), SlotStatus.ASSIGNED, pageReq);
		PagedResponse<InterviewSlot> response = new PagedResponse<>();
		response.setList(list);
		response.setTotalCount(repo.countByInterviewerIdAndStatus(UserUtils.getCurrentUser().getId(), SlotStatus.ASSIGNED));
		response.setPageSize(pageReq.getPageSize());
		response.setCurrentPage(pageReq.getPageNumber());
//		paged
		return response;
	}

//	@Override
//	public List<InterviewSlot> getSlotsByInterview(Long interviewId) throws Exception{
//		Interview interview = interviewRepo.getOne(interviewId);
//		return repo.findByInterview(interview);
//	}

	@Override
	@Transactional
	public InterviewSlot cancelSlot(Long slotId) throws Exception {
		boolean statusChanged = false;
		InterviewSlot interviewSlot = repo.getOne(slotId);
		Interview interview = interviewRepo.findOne(interviewSlot.getInterview().getId());
		interviewSlot.getChangeRequest().setApproved(true);
		interviewSlot.setStatus(SlotStatus.CANCELED);
		InterviewSlot newSlot = new InterviewSlot(interviewSlot);
		repo.save(newSlot);
		if(statusChanged){
			interview.setStatus(InterviewUtils.getInterviewStatus(interview.getSlots()));
			interview = interviewRepo.save(interview);
		}
		return repo.save(interviewSlot);
	}

	@Override
	public InterviewSlot changeTiming(Long slotId, Date newDate) {
		InterviewSlot interviewSlot = repo.getOne(slotId);
		interviewSlot.getChangeRequest().setApproved(true);
		interviewSlot.getTimeSlot().setStartTime(newDate);
		return repo.save(interviewSlot);
	}

	@Override
	public InterviewSlot rejectChange(Long slotId) {
		InterviewSlot interviewSlot = repo.getOne(slotId);
		interviewSlot.getChangeRequest().setApproved(false);
		return repo.save(interviewSlot);
	}

	@Override
	public List<InterviewSlot> acceptSlot(Long[] slotIds) throws SlotNotAvailableException {
		List<Long> ids = Arrays.asList(slotIds);
		List<InterviewSlot> interviewerSlots = repo.findByInterviewerIdAndStatusOrderByTimeSlot_startTimeAsc(UserUtils.getCurrentUser().getId(), SlotStatus.ASSIGNED, null);
		List<InterviewSlot> interviewSlots = repo.findAll(ids);
		interviewerSlots.addAll(interviewSlots);
		for(InterviewSlot interviewSlot : interviewSlots){
			Date start = interviewSlot.getTimeSlot().getStartTime();
//			Integer duration = interviewSlot.getDuration();
//			duration = duration+5;
			Date end = interviewSlot.getTimeSlot().getEndTime();//InterviewUtils.addMinutes(start, duration);
			for(InterviewSlot pivote : interviewerSlots){
				if(!interviewSlot.equals(pivote)){
					Date startP = pivote.getTimeSlot().getStartTime();
					Date endP = pivote.getTimeSlot().getEndTime();
					Calendar cal = Calendar.getInstance();
					cal.setTime(endP);
					cal.add(Calendar.MINUTE, 5);
					endP = cal.getTime();
					if(startP.compareTo(start) * start.compareTo(endP) >= 0 || startP.compareTo(end) * end.compareTo(endP) >= 0){
						throw new SlotNotAvailableException(interviewSlot.getId(), pivote.getId());
					}
				}
			}
			interviewSlot.setInterviewerId(UserUtils.getCurrentUser().getId());
			interviewSlot.setStatus(SlotStatus.ASSIGNED);
		}
		return repo.save(interviewSlots);
	}

	@Override
	public InterviewSlot requestChange(Long slotId, ChangeRequest changeRequest) {
		InterviewSlot interviewSlot = repo.findOne(slotId);
		if(changeRequest!=null)
			interviewSlot.setChangeRequest(changeRequest);
		return repo.save(interviewSlot);
	}

	@Override
	public InterviewSlot approveChanges(Long slotId) throws Exception {
		boolean statusChanged = false;
		InterviewSlot interviewSlot = repo.getOne(slotId);
		Interview interview = interviewRepo.findOne(interviewSlot.getInterview().getId());
		interviewSlot.getChangeRequest().setApproved(true);
		if(interviewSlot.getChangeRequest().getRequestType().equals(RequestType.PROPOSE)){
			interviewSlot.getTimeSlot().setStartTime(interviewSlot.getChangeRequest().getProposedTime());
		}else{
			interviewSlot.setStatus(SlotStatus.CANCELED);
			InterviewSlot newSlot = new InterviewSlot(interviewSlot);
			repo.save(newSlot);
			statusChanged = true;
		}
		interviewSlot.setChangeRequest(null);
		interviewSlot = repo.save(interviewSlot);
		if(statusChanged){
			interview.setStatus(InterviewUtils.getInterviewStatus(interview.getSlots()));
			interview = interviewRepo.save(interview);
		}
		return interviewSlot;
	}

}
