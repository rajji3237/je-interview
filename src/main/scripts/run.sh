#!/bin/bash

# This script runs the Middleware application

CS_ROOT="`dirname $0`"/..
CS_ROOT=`cd $CS_ROOT && pwd`

#echo "Running Directory: $CS_ROOT"

# To find out if this process is running or not
JAVA_PROCESS_PATTERN="${CS_ROOT}-Middleware"
APPNAME="Middleware Application"

####################################
# Start function
####################################
start() {
    echo "Starting $APPNAME ..."

    pid=$(ps -ef | grep "$JAVA_PROCESS_PATTERN" | grep -v grep | head -1 | awk '{print $2}' )
  
    if [ "X$pid" != "X" ]; then 
        echo "$APPNAME is already running with process id $pid"        
        exit 1
    fi
    
    JAR_NAME="je-interview-0.0.1-SNAPSHOT.jar"
	MAIN_CLASS="com.cs.je.interview.JeInterviewApplication"

	JAVA_OPTS="-Xms128m -Xmx512m\
	-XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled \
	-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=${CS_ROOT}/logs/heap_dump.hprof"
	
	JAVA_DEBUG_OPT="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,address=8040,suspend=n"

	LIB_PATH=$CS_ROOT/lib

	# Find all the jar files in the library path and concatenate them
	for i in $(ls ${LIB_PATH}/*.jar)  
	do
		CLASSPATH="$CLASSPATH:$i"  	
	done

	CLASSPATH=$CLASSPATH:$CS_ROOT/config
   
    nohup java -Xmx1536m -XX:HeapDumpPath=/home/jobezy/dumps -server $JAVA_OPTS $JAVA_DEBUG_OPT -Droot.dir=$CS_ROOT -D${JAVA_PROCESS_PATTERN} -classpath $CLASSPATH $MAIN_CLASS --spring.profiles.active=dev > $CS_ROOT/logs/stdout.log 2>&1 > $CS_ROOT/logs/stdout.log  &
    
    echo "Checking $APPNAME status ..."
    
    sleep 5    
    status
}

####################################
# Stop function
####################################
stop() {
    pid=$(ps -ef | grep "$JAVA_PROCESS_PATTERN" | grep -v grep | head -1 | awk '{print $2}' )
   
    if [ "X$pid" != "X" ]; then
        echo "Stopping $APPNAME process $pid ..."
        kill $pid
        sleep 10

        # Check if the process is really gone
        ps -ef | awk '{print $2}' | grep $pid > /dev/null
        ret=$?

        # If the return vaule is not 0, the process is gone
        if [ $ret -eq 0 ]; then
            echo "$APPNAME is still running as process $pid, force killing ..."
            kill -9 $pid
            sleep 5
        fi
        
        echo "$APPNAME stopped."
		
    else
        echo "$APPNAME is not running"
    fi
}


####################################
# Status function
####################################
status () {
    pid=$(ps -ef | grep "$JAVA_PROCESS_PATTERN" | grep -v grep | head -1 | awk '{print $2}' )
    if [ "X$pid" != "X" ]; then 
        echo "$APPNAME is running as process $pid"
    else
        echo "$APPNAME is not running."
    fi
}


####################################
# Restart function
####################################
restart() {
    echo "Restarting $APPNAME ..."
    stop
    start
}

####################################
# Main function
####################################
  
case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  status)
    status
    ;;  
  restart)
    restart
    ;;  
  *)
    echo $"Usage: $prog {start|stop|status|restart}"
    exit 1
esac

exit $?
